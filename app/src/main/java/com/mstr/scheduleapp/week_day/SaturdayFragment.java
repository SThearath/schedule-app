package com.mstr.scheduleapp.week_day;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mstr.scheduleapp.R;
import com.mstr.scheduleapp.adapter.RecyclerviewAdapter;
import com.mstr.scheduleapp.data.Schedule;

import java.util.ArrayList;
import java.util.List;


public class SaturdayFragment extends Fragment {
    RecyclerView recyclerView;
    List<Schedule> scheduleList = new ArrayList<>();
    View view;
    private static SaturdayFragment mondayFragment;
    public static SaturdayFragment newInstance(int page, String title) {
        if(mondayFragment ==null){
            return new SaturdayFragment();
        }
        return mondayFragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scheduleList.add(new Schedule("7","Meeting"));
        scheduleList.add(new Schedule("8","Android"));
        scheduleList.add(new Schedule("9","Web"));
        scheduleList.add(new Schedule("10","Game"));
        scheduleList.add(new Schedule("11","Relax"));
        scheduleList.add(new Schedule("12","Lunch"));
    }

    //draw layour
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_saturday,container,false);
        recyclerView = view.findViewById(R.id.re_sat);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(new RecyclerviewAdapter(view.getContext(),scheduleList));
        return view;
    }
}

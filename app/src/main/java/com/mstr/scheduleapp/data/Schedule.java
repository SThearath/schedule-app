package com.mstr.scheduleapp.data;


public class Schedule {
    String timePicker;
    String title;

    public Schedule(){}
    public Schedule(String timePicker, String title) {
        this.timePicker = timePicker;
        this.title = title;
    }

    public String getTimePicker() {
        return timePicker;
    }

    public void setTimePicker(String timePicker) {
        this.timePicker = timePicker;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package com.mstr.scheduleapp;

import android.app.Dialog;
import android.os.Build;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.RequiresApi;

import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mstr.scheduleapp.data.Schedule;

public class MyFAB extends AAH_FabulousFragment{
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("schedule");


    public static MyFAB newInstance() {
        MyFAB f = new MyFAB();
        return f;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.filter_sample_view, null);

        RelativeLayout rl_content = (RelativeLayout) contentView.findViewById(R.id.rl_content);
        contentView.findViewById(R.id.btn_add).setOnClickListener(v->{
            final EditText editText = contentView.findViewById(R.id.et_title);
            final TimePicker  timePicker= contentView.findViewById(R.id.time_picker);
            final ToggleButton mon = contentView.findViewById(R.id.tog_mon);
            final ToggleButton tue = contentView.findViewById(R.id.tog_tue);
            final ToggleButton wed = contentView.findViewById(R.id.tog_wed);
            final ToggleButton thu = contentView.findViewById(R.id.tog_thu);
            final ToggleButton fri = contentView.findViewById(R.id.tog_fri);
            final ToggleButton sat = contentView.findViewById(R.id.tog_sat);
            final ToggleButton sun = contentView.findViewById(R.id.tog_sun);
            Schedule schedule = new Schedule(timePicker.getHour()+":"+timePicker.getMinute(),editText.getText().toString());
            if(mon.isChecked()==true){
                myRef.child("monday").setValue(schedule);

            }
            if(tue.isChecked()==true){
                myRef.child("tuesday").setValue(schedule);

            }
            if(wed.isChecked()==true){
                myRef.child("wednesday").setValue(schedule);

            }
            if(thu.isChecked()==true){
                myRef.child("thursday").setValue(schedule);

            }
            if(fri.isChecked()==true){
                myRef.child("friday").setValue(schedule);

            }
            if(sat.isChecked()==true){
                myRef.child("saturday").setValue(schedule);

            }
            if(sun.isChecked()==true){
                myRef.child("sunday").setValue(schedule);
            }
//            editText.getText().toString();
            Toast.makeText(contentView.getContext(),"A"+timePicker.getHour()+":"+timePicker.getMinute(),Toast.LENGTH_LONG).show();
            closeFilter("save");
        });
        contentView.findViewById(R.id.btn_close).setOnClickListener(v->{
            closeFilter("closed");
        });

        //setup View Pager

        setAnimationDuration(300); //optional; default 500ms
        setPeekHeight(500); // optional; default 400dp
        setCallbacks((Callbacks) getActivity()); //optional; to get back result
        setViewMain(rl_content); //necessary; main bottomsheet view
        setMainContentView(contentView); // necessary; call at end before super
        super.setupDialog(dialog, style); //call super at last

    }
}

package com.mstr.scheduleapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mstr.scheduleapp.R;
import com.mstr.scheduleapp.data.Schedule;

import java.util.List;

public class RecyclerviewAdapter extends RecyclerView.Adapter<RecyclerviewAdapter.ViewHolder> {
    List<Schedule> scheduleList;
    Context context;

    public RecyclerviewAdapter(Context context,List<Schedule> scheduleList) {
        this.scheduleList = scheduleList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.re_custom,parent,false);
        ViewHolder holder = new ViewHolder(view);


        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tt.setText(scheduleList.get(position).getTitle());
        holder.tm.setText(scheduleList.get(position).getTimePicker());
        holder.ll.setOnClickListener(view -> {
            Toast.makeText(context,"Edit : "+position,Toast.LENGTH_LONG).show();
        });
        holder.ll.setOnLongClickListener(view -> {
            Toast.makeText(context,"Delete : "+position,Toast.LENGTH_LONG).show();
            return true;
        });
    }


    @Override
    public int getItemCount() {
        return scheduleList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tt,tm;
        LinearLayout ll;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tt = itemView.findViewById(R.id.title_text);
            tm = itemView.findViewById(R.id.time_text);
            ll = itemView.findViewById(R.id.ll_click_item);
        }
    }
}

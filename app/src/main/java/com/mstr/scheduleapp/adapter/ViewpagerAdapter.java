package com.mstr.scheduleapp.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mstr.scheduleapp.week_day.FridayFragment;
import com.mstr.scheduleapp.week_day.MondayFragment;
import com.mstr.scheduleapp.week_day.SaturdayFragment;
import com.mstr.scheduleapp.week_day.SundayFragment;
import com.mstr.scheduleapp.week_day.ThursdayFragment;
import com.mstr.scheduleapp.week_day.TuesdayFragment;
import com.mstr.scheduleapp.week_day.WednesdayFragment;

import java.util.List;

public class ViewpagerAdapter extends FragmentStatePagerAdapter {

    List<Fragment> fragmentList ;
        public ViewpagerAdapter( FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragmentList = fragments;

    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

            switch (position){
                case 0:
                    return MondayFragment.newInstance(1,"page 1");
                    case 1:
                    return TuesdayFragment.newInstance(2,"page 2");
                    case 2:
                    return WednesdayFragment.newInstance(3,"page 3");
                    case 3:
                    return ThursdayFragment.newInstance(4,"page 4");
                    case 4:
                    return FridayFragment.newInstance(5,"page 5");
                    case 5:
                    return SaturdayFragment.newInstance(6,"page 6");
                    case 6:
                    return SundayFragment.newInstance(7,"page 7");
                    default:
                        return fragmentList.get(position);
            }
//
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
//
//    @Nullable
//    @Override
//    public CharSequence getPageTitle(int position) {
//        return (CharSequence) fragmentList.get(position);
//    }
}

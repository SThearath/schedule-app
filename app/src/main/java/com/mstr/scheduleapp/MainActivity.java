package com.mstr.scheduleapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.fxn.BubbleTabBar;
import com.fxn.OnBubbleClickListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mstr.scheduleapp.adapter.RecyclerviewAdapter;
import com.mstr.scheduleapp.adapter.ViewpagerAdapter;
import com.mstr.scheduleapp.data.Schedule;
import com.mstr.scheduleapp.week_day.FridayFragment;
import com.mstr.scheduleapp.week_day.MondayFragment;
import com.mstr.scheduleapp.week_day.SaturdayFragment;
import com.mstr.scheduleapp.week_day.SundayFragment;
import com.mstr.scheduleapp.week_day.ThursdayFragment;
import com.mstr.scheduleapp.week_day.TuesdayFragment;
import com.mstr.scheduleapp.week_day.WednesdayFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AAH_FabulousFragment.Callbacks,OnBubbleClickListener,View.OnClickListener{

    public BubbleTabBar bubbleTabBar;
    public ViewPager viewPager;
    public ViewpagerAdapter viewpagerAdapter;

    public List<Fragment> fragmentList = new ArrayList<>();
    MyFAB myFAB;
    FloatingActionButton floatingActionButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        addFragment();
        setupUI();

    }

    private void setupUI() {
        //viewpage in floating btn
        myFAB = MyFAB.newInstance();
        myFAB.setParentFab(floatingActionButton);
        floatingActionButton.setOnClickListener(this);



        //viewpager
        viewpagerAdapter = new ViewpagerAdapter(getSupportFragmentManager(),fragmentList);
        viewPager.setAdapter(viewpagerAdapter);
        viewPager.setBackgroundColor(Color.WHITE);


        //tab setup
        bubbleTabBar.setBackgroundColor(Color.WHITE);
        bubbleTabBar.addBubbleListener(this);
        bubbleTabBar.setupBubbleTabBar(viewPager);

    }

    private void addFragment() {
        fragmentList.add(new MondayFragment());
        fragmentList.add(new TuesdayFragment());
        fragmentList.add(WednesdayFragment.newInstance(2,"Wednesday"));
        fragmentList.add(ThursdayFragment.newInstance(3,"Thursday"));
        fragmentList.add(FridayFragment.newInstance(4,"Friday"));
        fragmentList.add(SaturdayFragment.newInstance(5,"Saturday"));
        fragmentList.add(SundayFragment.newInstance(6,"Sunday"));
    }

    private void init() {
        bubbleTabBar = findViewById(R.id.bubbleTabBar);
        viewPager = findViewById(R.id.viewpager);
        floatingActionButton = findViewById(R.id.fab);
    }

    @Override
    public void onResult(Object result) {

    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBubbleClick(int i) {
        switch (i){
            case R.id.mon:
                viewPager.setCurrentItem(0);
                Toast.makeText(getApplicationContext(),"Monday",Toast.LENGTH_LONG).show();
                break;
            case R.id.tue:
                viewPager.setCurrentItem(1);
                Toast.makeText(getApplicationContext(),"Tuesday",Toast.LENGTH_LONG).show();
                break;
            case R.id.wed:
                viewPager.setCurrentItem(2);
                Toast.makeText(getApplicationContext(),"Wednesday",Toast.LENGTH_LONG).show();
                break;
            case R.id.thu:
                viewPager.setCurrentItem(3);
                Toast.makeText(getApplicationContext(),"Thursday",Toast.LENGTH_LONG).show();
                break;
            case R.id.fri:
                viewPager.setCurrentItem(4);
                Toast.makeText(getApplicationContext(),"Friday",Toast.LENGTH_LONG).show();
                break;
            case R.id.sat:
                viewPager.setCurrentItem(5);
                Toast.makeText(getApplicationContext(),"Saturday",Toast.LENGTH_LONG).show();
                break;
            case R.id.sun:
                viewPager.setCurrentItem(6);
                Toast.makeText(getApplicationContext(),"Sunday",Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.fab){
            myFAB.show(getSupportFragmentManager(), myFAB.getTag());
        }
    }

}
